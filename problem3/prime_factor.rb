# The prime factors of 13195 are 5, 7, 13 and 29.
# What is the largest prime factor of the number 600851475143 ?

require 'mathn'
require 'test/unit'

class PrimeFactor
	def initialize number
		@number = number
	end

	attr_reader :number

	def largest_number
		array = ::Prime.prime_division(number)
		array.flatten.uniq.sort.last
	end
end

class PrimeFactorTest < Test::Unit::TestCase
  def test_largest_number
    assert_equal(29, PrimeFactor.new(13195).largest_number)
    assert_equal(6857, PrimeFactor.new(600851475143).largest_number)
  end
end
