# A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

# a2 + b2 = c2
# For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.

# There exists exactly one Pythagorean triplet for which a + b + c = 1000.
# Find the product abc.

# rough attempt at implementing fibonacci method to generate pythagorean triples
# 1. Pick one odd number to be a.
# 1a. Square a.
# 2. b is equal to the sum of *odd numbers* before a.
# 3. If a squared = 49, then b squared = 1 + 3 + 5 + 7 + 9 + (...) + 47 = 576
# 4. c is equal to the sum of odd numbers that includes a.
# 5. c squared = 1 + 3 + 5 + 7 + 9 (...) + 47 + 49 = 625

class PythagoreanTriplet
  def initialize(sum_of_triplet)
    @sum_of_triplet = sum_of_triplet
  end

  attr_reader :sum_of_triplet, :hash

  def doit
    array = []
    odd_numbers = (3..sum_of_triplet).select{|num| num % 2 != 0 }
    odd_numbers.each do |i|
      a2 = i ** 2
      b2 = (1...a2).select{|num| num % 2 != 0 }.inject(:+)
      c2 = a2 + b2
      a = Math.sqrt(a2).to_i
      b = Math.sqrt(b2).to_i
      c = Math.sqrt(c2).to_i
      sum_of_abc = a + b + c
      array << [a,b,c,sum_of_abc]
    end
    array
  end
end

puts "#{PythagoreanTriplet.new(1000).doit}"
