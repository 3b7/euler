# If we list all the natural numbers below 10 that are
# multiples of 3 or 5, we get 3, 5, 6 and 9.
# The sum of these multiples is 23.
# Find the sum of all the multiples of 3 or 5 below 1000.

require 'test/unit'

class MultiplesOf
  def initialize upper_bound
    @upper_bound = upper_bound
  end

  attr_reader :upper_bound

  def sum
    total = 0

    (1...upper_bound).each do |i|
      total += i if (i % 3 == 0 || i % 5 == 0 )
    end

    total
  end
end

class MultiplesOfTest < Test::Unit::TestCase
  def test_sum
    assert_equal(23, MultiplesOf.new(10).sum)
    assert_equal(233168, MultiplesOf.new(1_000).sum)
  end
end
