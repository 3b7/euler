# A palindromic number reads the same both ways.
# The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
# Find the largest palindrome made from the product of two 3-digit numbers.

require 'test/unit'

class PalindromicNumber
  def initialize digit_size
    @digit_size = digit_size
  end

  attr_reader :digit_size

  def largest
    numbers = fetch_numbers
    palindromic_numbers = []
    numbers.each do |number|
      palindromic_numbers << numbers.collect{ |n| n * number }.reject{ |x| x.to_s != x.to_s.reverse }
    end
    palindromic_numbers.flatten.sort.last
  end

  private

  def fetch_numbers
    i = 0
    numbers = []
    loop do
      i += 1
      numbers << i if i.to_s.length == digit_size
      break if i.to_s.length > digit_size
    end
    numbers
  end
end

class PalindromicNumberTest < Test::Unit::TestCase
  def test_largest
    assert_equal(906609, PalindromicNumber.new(3).largest)
    assert_equal(9009, PalindromicNumber.new(2).largest)
  end
end
