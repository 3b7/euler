# 2520 is the smallest number that can be divided by each
# of the numbers from 1 to 10 without any remainder.
# What is the smallest positive number that is evenly divisible
# by all of the numbers from 1 to 20?

#
# FYI: the fastest way to do it is actually using `lcm` from
# the Integer class. but it's apparently against the spirit of
# the euler problems.
# => (1..20).inject(:lcm)

require 'test/unit'

class SmallestMultiple
  def initialize number
    @number = number
  end

  attr_reader :number

  def smallest_positive_number
		i = 0
		loop do
		  i += 20
      # NOTE: original implementation had increments/steps
      # of 1 making it this a slow algo. took about 25 mins
      # changing it to increments of 20 (bcs the number returned
      # must be divisble by 20) brought this down to under 2 mins
      break if is_divisble_by(i)
		end
		i
  end

  def is_divisble_by(i)
    a = []
    (1..number).to_a.each do |num|
      a << (i % num == 0)
    end
    a.uniq!
    a.size == 1 && a.last
  end
end

class SmallestMultipleTest < Test::Unit::TestCase
  def test_smallest_positive_number
    assert_equal(232792560, SmallestMultiple.new(20).smallest_positive_number)
  end
end
