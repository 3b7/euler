# The sum of the squares of the first ten natural numbers is,

# 1^2 + 2^2 + ... + 10^2 = 385
# The square of the sum of the first ten natural numbers is,

# (1 + 2 + ... + 10)^2 = 55^2 = 3025
# Hence the difference between the sum of the squares of the first
# ten natural numbers and the square of the sum is 3025 − 385 = 2640.

# Find the difference between the sum of the squares of the first
# one hundred natural numbers and the square of the sum.

require 'active_support/core_ext/enumerable'
require 'test/unit'

class SumSquareDifference
	def initialize(max_number)
		@max_number = max_number
	end

	def diff
		square_of_sum - sum_of_squares
	end

	private

	attr_reader :max_number

	def sum_of_squares
		a = []
		1.upto(max_number).each do |i|
			a << i ** 2
		end
		a.sum
	end

	def square_of_sum
		(1..max_number).to_a.sum ** 2
	end
end

class SumSquareDifferenceTest < Test::Unit::TestCase
	def test_diff
		assert_equal(2640, SumSquareDifference.new(10).diff)
		assert_equal(25164150, SumSquareDifference.new(100).diff)
	end
end
